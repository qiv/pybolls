bolls
=====
Python modules to simulate center-dynamics (aka spheroid) models, like the following simulation of [cell sorting](https://en.wikipedia.org/wiki/Differential_adhesion_hypothesis):

![Cell sorting simulation](sorting.gif)

To install the required packages run `$ pip install -r requirements.txt`. For usage see the examples in `if __name__ == '__main__'` in the corresponding `bolls/module.py`, which can be executed with `$ python -m bolls.module`. For further examples see the `models` folder.
