"""Wrappers for scipy ode class"""
import sys
import time
from collections import OrderedDict

import numpy as np
import pandas as pd
from scipy.integrate import ode


def uniform_sphere(n_cells=50, r_min=0.5):
    """Uniformly distribute n_cells randomly in s sphere of radius r"""
    r = (n_cells/0.75)**(1/3)*r_min/2 # Sphere packing
    np.random.seed(0)
    r = r*np.random.rand(n_cells)**(1/3)
    theta = np.random.rand(n_cells)*2*np.pi
    phi = np.arccos(2*np.random.rand(n_cells) - 1)
    return np.column_stack((
        r*np.sin(theta)*np.sin(phi),
        r*np.cos(theta)*np.sin(phi),
        r*np.cos(phi)))


def _hr_time(seconds):
    """Return human readable time"""
    units = OrderedDict(
        sorted({'s': 1, 'm': 60, 'h': 60*60}.items(), key=lambda t: t[0]))
    hr_times = []
    for unit in units:
        if seconds//units[unit] != 0:
            hr_times.append(str(seconds//units[unit]) + unit)
            seconds -= (seconds//units[unit])*units[unit]
    if len(hr_times) == 0:
        return '{:2.2f}s'.format(seconds)
    return ' '.join(hr_times[:2])


def scipy_ode(force, X0, t0, tf, dt, f_params=False, method='dopri5'):
    """Integrate force(t, X) using the scipy.integrate.ode class"""
    solver = ode(force).set_integrator(method)
    solver.set_initial_value(X0.ravel(), t0)
    if f_params:
        solver.set_f_params(f_params)

    i = 0; n_steps = (tf - t0)/dt; positions = pd.DataFrame()
    t_start = time.time()
    while solver.successful() and solver.t < tf:
        solver.integrate(solver.t + dt)
        sys.stdout.write('\rIntegrating {}, {:3.0f}% done'.format(
            force.__name__, 100*i/n_steps))
        sys.stdout.flush()
        current = pd.DataFrame(solver.y.reshape(-1, 3), columns=('X', 'Y', 'Z'))
        current['Timestep'] = i; i = i + 1
        current['Cell_ID'] = np.arange(len(X0.ravel())/3)
        positions = positions.append(current)
    print(', ' + _hr_time(time.time() - t_start) + ' taken.')

    return positions


if __name__ == '__main__':
    """Compare different solvers"""
    import seaborn as sns
    import matplotlib.pyplot as plt

    sns.set(style='ticks')

    def leonard_jones(_, X):
        """Forces for the potential U = (r_0/r)**12 - 2*(r_0/r)**6"""
        X = X.reshape(-1, 3)
        F = np.zeros(X.shape)
        for i, x in enumerate(X):
            r_min = 0.5*2**(1/6)
            r = X - x
            norm_r = np.linalg.norm(r, axis=1)
            F[i] -= r_min**12*np.nansum(r/norm_r[:, None]**13, axis=0)
            F[i] += 2*r_min**6*np.nansum(r/norm_r[:, None]**7, axis=0)
        return F.ravel()

    X0 = uniform_sphere()

    def melt(df):
        return pd.melt(df[['X', 'Y', 'Z', 'Timestep']], id_vars=['Timestep'])

    methods = ['lsoda', 'vode', 'dop853', 'dopri5']
    for i, method in enumerate(methods):
        t0 = time.time()
        positions = scipy_ode(
            leonard_jones, X0, 0, 5, 0.1, method=method)
        col = method + ', ' + '{:2.2f} s'.format(time.time() - t0)
        if i == 0:
            ref = 'Reference (' + col + ')'
            comp = melt(positions)[['Timestep', 'value']]
            comp.columns = ['Timestep', ref]
        else:
            comp[col] = np.abs((melt(positions)['value'] - comp[ref]))

    stats = comp.groupby('Timestep').describe().unstack()

    plt.xlabel('Timestep')
    plt.ylabel('Distance to ' + comp.columns[1])
    plt.gca().set_yscale('log')
    for i, method in enumerate(comp.columns[2:]):
        color = sns.color_palette(n_colors=i+1)[-1]
        plt.plot(stats[method]['50%'], label=method)
        plt.fill_between(
            stats[method].index, stats[method]['25%'], stats[method]['75%'],
            alpha=.2, color=color)
        plt.fill_between(
            stats[method].index, stats[method]['min'], stats[method]['max'],
            alpha=.2, color=color)
    sns.despine()
    plt.legend(loc='upper left')
    plt.tight_layout()
    plt.show()
