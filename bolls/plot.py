"""Utilities to analyze and visualize center dynamics models"""
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import matplotlib.animation as animation
from scipy.integrate import cumtrapz


sns.set(style='ticks', palette='deep')


def potential(force):
    """Probe force and plot the integrated potential"""
    r = np.linspace(0.2, 1.2, 100)
    F = np.empty(len(r))
    for i in range(len(r)):
        X = np.zeros(6)
        X[3] = r[i]
        F[i] = force(0, X)[0]
    U = cumtrapz(F, r, initial=0)

    plt.xlabel('Distance')
    plt.ylabel('Potential')
    plt.plot(r, U)
    plt.ylim((U.min(), 1.1*U[-1]  - 0.1*U.min()))
    sns.despine()
    plt.tight_layout()
    plt.show()


def _equalize_axis3d(source_ax, zoom=1, target_ax=None):
    """after http://stackoverflow.com/questions/8130823/
    set-matplotlib-3d-plot-aspect-ratio"""
    if target_ax == None:
        target_ax = source_ax
    elif zoom != 1:
        print('Zoom ignored when target axis is provided.')
        zoom = 1
    source_extents = np.array(
        [getattr(source_ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    target_extents = np.array(
        [getattr(target_ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    spread = target_extents[:, 1] - target_extents[:, 0]
    max_spread = max(abs(spread))
    r = max_spread/2
    centers = np.mean(source_extents, axis=1)
    for center, dim in zip(centers, 'xyz'):
        getattr(source_ax, 'set_{}lim'.format(dim))(center - r/zoom, center + r/zoom)


def animate_scatter(positions, color_code=False, save_as=False, elev=30):
    """Animate scatter plot of cell centers"""
    fig = plt.figure(figsize=(6, 6))
    ax = p3.Axes3D(fig)
    ax.set_aspect('equal')
    ax.axis('off')

    current = positions[positions['Timestep'] == 0].copy()
    if color_code:
        for i, code in enumerate(positions[color_code].unique()):
            current.loc[current[color_code] == code, 'Color'] = \
                sns.color_palette().as_hex()[i]
    else:
        current['Color'] = sns.color_palette().as_hex()[0]
    scat = ax.scatter(
        current['X'], current['Y'], current['Z'], c=current['Color'])

    def update_plot(i, positions, scat):
        current = positions[positions['Timestep'] == i]
        scat._offsets3d = (current['X'], current['Y'], current['Z'])
        return scat

    anim = animation.FuncAnimation(
        fig, update_plot, fargs=(positions, scat),
        frames=len(positions['Timestep'].unique()))

    _equalize_axis3d(ax, zoom=2)
    ax.view_init(elev)
    if save_as:
        anim.save(save_as + '.mp4', fps=15, extra_args=['-vcodec', 'libx264'])
    else:
        plt.show()


def nn_distance(positions, max_nn=50, eq_steps=5):
    """Plot distance to the max_nn nearest neighbours after eq_steps"""
    n_cells = len(positions['Cell_ID'].unique())
    if max_nn >= n_cells:
        max_nn = n_cells - 1
    nn_distances = pd.DataFrame(
        index=np.arange(len(positions)), columns=np.arange(max_nn + 1)[1:], dtype=float)
    max_index = 0
    for _, current in positions[positions['Timestep'] > eq_steps].groupby('Timestep'):
        for _, position in current.iterrows():
            r = current[['X', 'Y', 'Z']] - position[['X', 'Y', 'Z']]
            norm_r = np.linalg.norm(r, axis=1)
            norm_r.sort()
            nn_distances.iloc[max_index, :] = norm_r[1:max_nn + 1]
            max_index += 1
    stats = nn_distances.describe()
    plt.xlabel('Neighbor')
    plt.ylabel('Distance')
    plt.plot(stats.columns, stats.loc['50%', :])
    plt.fill_between(
        stats.columns, stats.loc['25%', :], stats.loc['75%', :], alpha=0.2)
    plt.fill_between(
        stats.columns, stats.loc['min', :], stats.loc['max', :], alpha=0.2)
    sns.despine()
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    """Simulate and visualize aggregation of points connected by springs"""
    from bolls import solve

    def spring(_, X):
        """Spring forces for Hookes's law U = (r - l)**2"""
        X = X.reshape(-1, 3)
        F = np.zeros(X.shape)
        l = 0.5
        for i, x in enumerate(X):
            r = X - x
            norm_r = np.linalg.norm(r, axis=1)
            F[i] += 2*np.nansum(r*((norm_r - l)/norm_r)[:, None], axis=0)
        return F.ravel()

    potential(spring)

    X0 = solve.uniform_sphere()

    positions = solve.scipy_ode(spring, X0, t0=0, tf=5, dt=0.1)

    animate_scatter(positions)
    nn_distance(positions)
