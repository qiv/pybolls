"""Simulate mechanical properties of mesenchymal cells"""
import numpy as np
import scipy.spatial as spatial
import theano
from theano import tensor as T


def step(x, dx=0.1):
    """Smooth transition from step(x < 0) = 0 to step(x > 0) = 1 over dx"""
    x = np.clip((x + dx/2)/dx, 0, 1)
    return x*x*(3 - 2*x)


def squeeze(t, X):
    """Forces for plate below and plate squeezing from top"""
    X = X.reshape(-1, 3)
    F = np.zeros(X.shape)
    F[:, 2] += 1000*step(-1.5 - X[:, 2]) # Floor
    if t > 3 and t < 7:
        F[:, 2] -= 1000*step(X[:, 2] - (2 - (t - 3)/2)) # Squeeze from top
    return F.ravel()


def leonard_jones(t, X):
    """Calculate forces for the potential U = (r_0/r)**12 - 2*(r_0/r)**6"""
    r_min = 0.5*2**(1/6)
    X = X.reshape(-1, 3)
    F = np.zeros(X.shape)
    for i, x in enumerate(X):
        r = X - x
        norm_r = np.linalg.norm(r, axis=1)
        F[i] -= r_min**12*np.nansum(r/norm_r[:, None]**13, axis=0)
        F[i] += 2*r_min**6*np.nansum(r/norm_r[:, None]**7, axis=0)
    return F.ravel()


class CubicKNN(object):
    """U = strength*(r_min - r)*(r_max - r)**2 between k nearest neighbors"""
    def __init__(self, k=50, r_min=0.5, r_max=1, strength=50):
        assert 8*r_min*r_max - 3*r_min**2 - 2*r_max**2 > 0, \
            'min(F(r) + F(2r)) < 0: cells interact across each other and collapse'
        self.k = k
        self.N = None
        self.__name__ = 'Cubic Pot. to {} NNs'.format(self.k)
        R = T.tensor3('R', dtype='floatX')
        norm_R = T.minimum(R.norm(2, axis=2), r_max)
        norm_F = 2*(r_min - norm_R)*(r_max - norm_R) + (r_max - norm_R)**2
        F = strength*T.sum(R*(norm_F/norm_R).dimshuffle(0, 1, 'x'), axis=1)
        self.f = theano.function([R], F.ravel(), allow_input_downcast=True)

    def __call__(self, t, X):
        if self.N is None:
            self.find_neighborhoods(X)
        X = X.reshape(-1, 3)
        R = X[:, None, :]  - X[self.N]
        return self.f(R)

    def find_neighborhoods(self, X0):
        X0 = X0.reshape(-1, 3)
        N = np.empty((len(X0), self.k), dtype=int)
        tree = spatial.cKDTree(X0)
        self.N = tree.query(X0, self.k + 1)[1][:, 1:]


if __name__ == '__main__':
    """N-body problem for squeezed cells"""
    from bolls import plot, solve

    # plot.potential(leonard_jones)
    # cubic = CubicKNN(k=1)
    # plot.potential(cubic)

    X0 = solve.uniform_sphere(200)

    # positions = solve.scipy_ode(leonard_jones, X0, t0=0, tf=15, dt=0.1/2)

    cubic_knn = CubicKNN(k=199)
    # positions = solve.scipy_ode(cubic_knn, X0, t0=0, tf=15, dt=0.1/2)

    def squeezed_cubic_knn(t, X): return cubic_knn(t, X) + squeeze(t, X)
    positions = solve.scipy_ode(squeezed_cubic_knn, X0, t0=0, tf=15, dt=0.1)

    # def squeezed_lj(t, X): return leonard_jones(t, X) + squeeze(t, X)
    # positions = solve.scipy_ode(squeezed_lj, X0, t0=0, tf=15, dt=0.1/2)

    plot.animate_scatter(positions)

    plot.nn_distance(positions, eq_steps=50)
