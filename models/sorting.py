"""Cell sorting and intermingling"""
import numpy as np


def two_cell_types_lj(t, X, sort=False):
    """Calculate forces for sorting/intermingling cell types A and B"""
    X = X.reshape(-1, 3)
    F = np.zeros(X.shape)

    r_min = 0.5*2**(1/6)
    e_AB = 1/3
    half = np.ones(len(X)/2)
    if sort:
        eps = np.concatenate((half/e_AB, e_AB*half)).reshape(-1, 1)
    else:
        eps = np.concatenate((half, half/e_AB)).reshape(-1, 1)

    for i, x in enumerate(X):
        if i == len(X):
            if sort:
                eps = np.concatenate((e_AB*half, half)).reshape(-1, 1)
            else:
                eps = eps[::-1]
        r = X - x
        norm_r = np.linalg.norm(r, axis=1)
        F[i] -= r_min**12*np.nansum(eps*r/norm_r[:, None]**13, axis=0)
        F[i] += 2*r_min**6*np.nansum(eps*r/norm_r[:, None]**7, axis=0)
    return F.ravel()


if __name__ == '__main__':
    """Simulate two cell types aggregating to checkerboard and then sort"""
    from bolls import plot, solve

    plot.potential(two_cell_types_lj)

    n_cells = 100
    X0 = solve.uniform_sphere(n_cells)

    mix = solve.scipy_ode(two_cell_types_lj, X0, t0=0, tf=5, dt=0.1)

    X0 = mix[mix['Timestep'] == mix['Timestep'].max()][['X', 'Y', 'Z']].as_matrix()

    sort = solve.scipy_ode(two_cell_types_lj, X0, t0=5, tf=10, dt=0.1, f_params=True)
    sort['Timestep'] += mix['Timestep'].max()

    positions = mix.append(sort[sort['Timestep'] > mix['Timestep'].max()])
    positions.loc[positions['Cell_ID'] < n_cells/2, 'Cell Type'] = 'A'
    positions.loc[positions['Cell_ID'] >= n_cells/2, 'Cell Type'] = 'B'

    plot.animate_scatter(positions, color_code='Cell Type')
